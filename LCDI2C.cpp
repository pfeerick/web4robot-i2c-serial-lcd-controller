/**
 * \file LCDI2C.cpp
 * \brief Class logic and procedural code
 */
 
#include "LCDI2C.h"
  
#include <Wire.h> //why bother including this?
#include <string.h> //needed for strlen()

#if ARDUINO >= 100
	#include "Arduino.h"   //Arduino IDE 1.0 onwards
#else
	#include "WConstants.h" //pre Arduino IDE 1.0
#endif

//--------------------------------------------------------

LCDI2C::LCDI2C (uint8_t row, uint8_t col, uint8_t fwVer, uint8_t addr, uint8_t disp)
{
	//store the pameters given into memory
	rows = row;
	cols = col;
	fw_version = fwVer;
	i2c_address = addr;
	display = disp; 
	
	//make sure the row and col values are within valid range
	if (rows < 1 || rows > 4){
		
		rows = 2;
	}
	
	if (cols < 1 || cols > 40){
		
		cols = 16;
	}
}

void LCDI2C::begin()
{
	Wire.begin();

	on();
	cursor(false);
	underline(false);
	clear();
	home();
	setKeypadMode(keypad_mode);
}

inline size_t LCDI2C::write(uint8_t value) 
{
  	Wire.beginTransmission(i2c_address);
	Wire.write(value);
	Wire.endTransmission();
	delayMicroseconds(DELAY_CMD);
	return 1; // assume sucess
}

void LCDI2C::command(uint8_t param1) 
{
	Wire.beginTransmission(i2c_address);
	Wire.write(0xFE);
	Wire.write(param1);
	Wire.endTransmission();
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::command(uint8_t param1, uint8_t param2)
{
	Wire.beginTransmission(i2c_address);
	Wire.write(0xFE);
	Wire.write(param1);
	Wire.write(param2);
	Wire.endTransmission();
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::command(uint8_t param1, uint8_t param2, uint8_t param3)
{
	Wire.beginTransmission(i2c_address);
	Wire.write(0xFE);
	Wire.write(param1);
	Wire.write(param2);
	Wire.write(param3);
	Wire.endTransmission();
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::command(uint8_t param1, uint8_t param2, uint8_t param3, uint8_t param4) 
{
	Wire.beginTransmission(i2c_address);
	Wire.write(0xFE);
	Wire.write(param1);
	Wire.write(param2);
	Wire.write(param3);
	Wire.write(param4);
	Wire.endTransmission();
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::command(uint8_t param1, uint8_t param2, uint8_t param3, uint8_t param4, uint8_t param5) 
{
	Wire.beginTransmission(i2c_address);
	Wire.write(0xFE);
	Wire.write(param1);
	Wire.write(param2);
	Wire.write(param3);
	Wire.write(param4);
	Wire.write(param5);
	Wire.endTransmission();
	delayMicroseconds(DELAY_CMD);
}

// LCD Configuration Commands -------------------------------------------------

 void LCDI2C::changeAddress(uint8_t addr)
 {
	command(0x01,addr);
	delayMicroseconds(DELAY_CONFIG);
 }

 void LCDI2C::changeBaud(uint8_t baud)
 {
	command(0x02,baud);
	delayMicroseconds(DELAY_CONFIG);
 }
 
 
bool LCDI2C::setBacklight(uint8_t val)
{
	//as long as value given is not invalid...
	if ((val < LCDI2C_BRIGHTNESS_MIN) || (val > LCDI2C_BRIGHTNESS_MAX))
	{
		return 1;
	}

	command(0x03,val);
	delayMicroseconds(DELAY_CONFIG);
	return 0;
}

bool LCDI2C::setContrast(uint8_t val)
{
	//as long as value given is not invalid...
	if ((val < LCDI2C_CONTRAST_MIN) || (val > LCDI2C_CONTRAST_MAX))
	{
		return 1;
	}
	
	command(0x04,val);
	delayMicroseconds(DELAY_CONFIG);
	return 0;
}

void LCDI2C::saveStartupScreen() 
{
	command(0x05);
	delayMicroseconds(DELAY_CONFIG);
}

void LCDI2C::displayFirmwareVersion()
{
	command(0x06);
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::displaySerialBaud()
{
	command(0x07);
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::displayI2Caddress()
{
	command(0x08);
	delayMicroseconds(DELAY_CMD);
}

// LCD Display Commands -------------------------------------------------------

void LCDI2C::on(){

	command(0x0A);
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::off()
{
	command(0x0B);
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::setCursor(uint8_t y, uint8_t x)
{

	command(0x0C,y,x);
	delayMicroseconds(DELAY_DISPLAY);
}


void LCDI2C::home()
{
	if (fw_version >= 140)
	{
		command(0x0D); //introduced in FW 1.4
	}
	else
	{
		setCursor(0,0);
	}

	delayMicroseconds(DELAY_DISPLAY);  
}

void LCDI2C::underline(bool state)
{
	if (state == true)
	{
		command(0x0E);
	}
	else
	{
		command(0x0F);
	}
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::left()
{
	command(0x10);
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::right()
{
	command(0x11);
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::cursor(bool state)
{
	if (state == true)
	{
		command(0x12);
	}
	else
	{
		command(0x13);
	}
	delayMicroseconds(DELAY_CMD);
}

void LCDI2C::clear()
{
	command(0x14);
	delayMicroseconds(DELAY_DISPLAY);
}

void LCDI2C::printString(const char c[])
{
	uint8_t len;

	//while there's still data, keep looping
	while (*c)
	{
		len = min(strlen(c), LCDI2C_MAX_STRLEN);
		
		//start transmitting character string
		Wire.beginTransmission(i2c_address);
		Wire.write(0xFE);
		Wire.write(0x15);
		Wire.write(len);
		
		//until the max string length is reached, keep sending
		while (len--)
		{			
			Wire.write(*c++);
		}
		
		//close the transmission
		Wire.endTransmission();
		
		//more to send, wait a bit
		if (*c)
		{
			delayMicroseconds(DELAY_CMD);	
		}
	}
	delayMicroseconds(DELAY_CMD);
}

// Bar Graphs and Custom Characters -------------------------------------------

uint8_t LCDI2C::initBG(uint8_t graphType)
{
	switch (graphType)
	{
		case 0:
			command(0x16,0x00);
			break;
		case 1:
			command(0x16,0x01);
			break;
		case 2:
			command(0x18);
			break;
		default:
			return 1;
	}
	
	delayMicroseconds(DELAY_DISPLAY);
	return 0;
}

uint8_t LCDI2C::drawBG(uint8_t graphType, uint8_t row, uint8_t col, uint8_t len,  uint8_t end)
{
	if (col > cols || col < 0)
		return 1;

	if (row > rows || col < 0)
		return 1;

	switch(graphType)
	{
		case 0:
			if (len > cols || len < 0)
				return 1;

			command(0x17,row,col,len,map(end,0,100,0,len*5));
			delayMicroseconds(DELAY_CMD);
			break;
			
		case 1:
			command(0x17,row,col,0x01,constrain(end,0,5));
			delayMicroseconds(DELAY_CMD);
			break;
			
		case 2:
			
			if (len > rows || len < 0)
				return 1;

			command(0x19,row,col,len,map(end,0,100,0,len*8));
			delayMicroseconds(DELAY_CMD);
			break;
		default:
			return 1;
	}
	return 0;
}

void LCDI2C::customCharacter(uint8_t location, uint8_t charmap[])
{
	uint8_t i;

	//start sending the custom character
	Wire.beginTransmission(i2c_address);
	Wire.write(0xFE);
	Wire.write(0x1A);
	location &= 0x7;
	Wire.write(location);
	
	//until we've reached the last row of the character, keep sending
	for (i = 0; i < LCDI2C_CUSTOM_CHAR_SIZE; i++)
	{
		Wire.write(charmap[i]);
	}
	
	///finished!
	Wire.endTransmission();
	delayMicroseconds(DELAY_CMD);
}

// Keypad Commands ------------------------------------------------------------

bool LCDI2C::setKeypadMode(uint8_t mode)
{
	if (mode >= 0 && mode <= 3 && fw_version >= 140)
	{
		command(0x1C,mode);
		delayMicroseconds(DELAY_CMD);
		return 0;
	}
	else
	{
		return 1;
	}
}

uint8_t LCDI2C::readKeypad ()
{
	uint8_t data = 0;

	// send keypad read command
	command(0x1B);
	
	// wait for lcd controller to respond
	delayMicroseconds(DELAY_KEYPAD);

	// receive keypad data
	Wire.requestFrom(i2c_address, uint8_t(1));

	if (Wire.available()) 
	{
		data = Wire.read();
	}
	
	delayMicroseconds(DELAY_CMD);
	
	return data;
}

uint8_t LCDI2C::readDigitalInputs()
{
	if (fw_version >= 140)
	{
		uint8_t data = 0;

		//  Send digital input read command
		command(0x1D);
		delayMicroseconds(DELAY_KEYPAD);; //is this needed?

		//  Connect to device and request byte
		Wire.beginTransmission(i2c_address);
		Wire.requestFrom(i2c_address, uint8_t(1));

		if (Wire.available()) 
		{
			data = Wire.read();
		}
		
		delayMicroseconds(DELAY_CMD);; 
		
		return data;
	}
	else
	{
		return 0;
	}
}

void LCDI2C::writeDigitalOutputs(uint8_t data)
{
	if (fw_version >= 140)
	{
		command(0x1E,data);
		delayMicroseconds(DELAY_CMD);
	}
}

// Other Commands -------------------------------------------------------------
uint8_t LCDI2C::readBacklight()
{
	uint8_t data = 0;

	//  Send brightness read command
	command(0x1F);
	delayMicroseconds(DELAY_CONFIG);; //is this needed?

	//  Connect to device and request byte
	Wire.beginTransmission(i2c_address);
	Wire.requestFrom(i2c_address, uint8_t(1));

	if (Wire.available()) 
	{
		data = Wire.read();
	}
	
	delayMicroseconds(DELAY_CMD);; //is this needed?
	
	return data;
}

uint8_t LCDI2C::readContrast()
{
	uint8_t data = 0;

	//  Send constrast read command
	command(0x20);
	delayMicroseconds(DELAY_CONFIG);; //is this needed?

	//  Connect to device and request byte
	Wire.beginTransmission(i2c_address);
	Wire.requestFrom(i2c_address, uint8_t(1));

	if (Wire.available()) 
	{
		data = Wire.read();
	}
	
	delayMicroseconds(DELAY_CMD);; //is this needed?
	
	return data;
}

