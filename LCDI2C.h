/** 
 * \file LCDI2C.h
 * \brief Class defintion and documentation
 *
 * Arduino library for the Web4Robots Serial/I2C LCD backpack (operating in I2C mode)
 * 
 * LCDI2C v0.8 27/Apr/2012 peter.feerick@gmail.com	http://www.peterfeerick.com.au
 *
 * Attribution
 * ===========
 * Based on initial works by Dale Wentz with contributions from Ran Talbott.
 *
 * Pin Configuration
 * =================
 *
 * Using the I2C (Wiring) interface
 * - A4 - SDA
 * - A5 - SCL
 *
 * Usage
 * =====
 *
 * See the examples folder of this library distribution.
 */
 
/**
 * \mainpage I2CLCD Manual
 *
 * Some general info.
 * 
 * This manual is divided in the following sections:
 * - \subpage intro
 * - \subpage advanced "Advanced usage"
 * 
 */

 //-----------------------------------------------------------

/** \page intro Introduction
This page introduces the user to the topic.
Now you can proceed to the \ref advanced "advanced section".
*/

//-----------------------------------------------------------

/** \page advanced Advanced Usage
This page is for advanced users.
Make sure you have first read \ref intro "the introduction".
*/

#ifndef LCDI2C_h
#define LCDI2C_h

#include "Print.h"

#define DELAY_CMD 					300 	/**< Minimum delay before next instruction can be given after a normal commands */
#define DELAY_CONFIG 				50000 	/**< Minimum delay before next instruction can be given after a configuration related command (due to EEPROM speed) */
#define DELAY_DISPLAY 				2000 	/**< Minimum delay before next instruction can be given after a display or display memory command */
#define DELAY_KEYPAD    			50000 	/**< Minimum delay before next instruction can be given after a keypad related command */

#define LCDI2C_BRIGHTNESS_MIN		0 		/**< Minimum LCD brightness */
#define LCDI2C_BRIGHTNESS_MAX		250 	/**< Maximum LCD brightness */
#define LCDI2C_BRIGHTNESS_DEFAULT   60 		/**< Module default LCD brightness */

#define LCDI2C_CONTRAST_MIN			0 		/**< Minimum LCD contrast */
#define LCDI2C_CONTRAST_MAX			100 	/**< Maximum LCD contrast */
#define LCDI2C_CONTRAST_DEFAULT  	20		/**< Module default LCD contrast */

#define LCDI2C_NUM_CUSTOM_CHARS 	8 		/**< Number of custom characters that can be stored */
#define LCDI2C_CUSTOM_CHAR_SIZE 	8  		/**< Height of custom characters */

#define LCDI2C_MAX_STRLEN			40  	/**< Maximum string length for the printString function */

/** 
 * \brief 		Main LCDI2C class
 * \details		Arduino library for the Web4Robots Serial/I2C LCD backpack (operating in I2C mode)
 * \author		Peter Feerick (current maintainer)
 * \author		Dale Wentz (original author)
 * \author		Ran Talbott (contributor)
 * \copyright	GNU Public License
 * \date		2012-MAY-29
 */
class LCDI2C : public Print 
{
	public: 

		/** 
		 * Class constructor
		 * 
		 * \param row Number of rows on the LCD
		 * \param col Number of columns on the LCD 
		 * \param fwVer Version of the firmware on the serial backpack... used for compatability (defaults to 140, as in version 1.40)
		 * \param addr I2C slave address (defaults to '0x4C')
		 * \param disp Display number (for when using multiple displays) (defaults to 1)
		 */
		LCDI2C (uint8_t row, uint8_t col, uint8_t fwVer = 140, uint8_t addr = 0x4C, uint8_t disp = 1);

		/** 
		 * Initialise the LCD and makes that:
		 * - the LCD display is on
		 * - the blinking cursor is not enabled
		 * - the underline cursor is not enabled
		 * - clears the screen
		 * - sets the cursor position to the home (0,0) position
		 */
		void begin();
		
		/** 
		 * Overload of the Print::write operator
		 */
		virtual size_t write(uint8_t value);	
		using Print::write;

		/** \addtogroup configCommands LCD Configuration Commands 
		*  @{
		*/
	
		/** 
		 * Sets the I2C address, which must be between 0x42 & 0x5A ('A' - 'Z'). 
		 * Default 0x4C
		 * 
		 * \param addr New I2C address
		 */
		void changeAddress(uint8_t addr);
		
		/** 
		 * Sets the serial communications rate.
		 * 
		 * \param baud 
		 * - 1 = 2400
		 * - 2 = 4800
		 * - 3 = 9600 (default)
		 * - 4 = 19200
		 */
		void changeBaud(uint8_t baud);
		
		/**
		 * Set the backlight level (saves to EEPROM)
		 * 
		 * \param val Value between 0 and 255 representing the LCD brightness setting
		 * \return Returns 0 if valid value given, otherwise returns 1
		 */
		bool setBacklight(uint8_t val);
		
		/** 
		 * Set the contrast level (saves to EEPROM)
		 *
		 * \param val Value between 0 and 100 representing the LCD contrast setting
		 * \return Returns 0 if valid value given, otherwise returns 1
		 */
		bool setContrast(uint8_t val);		
		
		/** 
		 * Save the current screen content as the splash screen in internal EEPROM
		 */
		void saveStartupScreen();
		
		/** 
		 * Displays the firmware version number on the first line of the LCD display
		 * 
		 * **Caution:** Outputs to the first line the LCD, disregarding current cursor
		 *              position or existing content.
		 */
		
		void displayFirmwareVersion();

		/** 
		 * Displays the current serial baud rate on the first line of the LCD display
		 * 
		 * **Caution:** Outputs to the first line the LCD, disregarding current cursor
		 *              position or existing content.
		 */
		void displaySerialBaud();

		/** 
		 * Displays the current I2C address on the first line of the LCD display
		 * 
		 * **Caution:** Outputs to the first line the LCD, disregarding current cursor
		 *              position or existing content.
		 */
		void displayI2Caddress();
		
		/** @}*/
		
		/** \addtogroup displayCommands LCD Display Commands 
		*  @{
		*/
			
		/**
		 * Turn the LCD display on
		 */
		void on();

		/** 
		 * Turn the LCD display off
		 */
		void off();
		
		/**
		 * Moves the cursor to the specified co-ordinates
		 *
		 * \param[in] y	Cursor row
		 * \param[in] x	Cursor column
		 */
		void setCursor(uint8_t y, uint8_t x);
		
		/** 
		 * Move cursor to home 
		 * 
		 * **Note:** This feature was intoduced in FW version 1.4, so code will 
		 *           automatically use setCursor(0,0) if older firmware is specified.
		 */
		void home();
		
		/** 
		 * Turn the underline cursor on or off, where the next character will be displayed
		 *
		 * \param[in]	state	Underline on or off
		 */
		void underline(bool state);
		
		/** 
		 * Move the cursor left 1 space
		 */
		void left();
		
		/** 
		 * Move the cursor right 1 space
		 */
		void right();
		
		/** 
		 * Turn the blinking cursor on or off, where the next character will be displayed
		 *
		 * \param[in]	state	Cursor on or off
		 */
		void cursor(bool state);
		
		/** 
		 * Clear the screen
		 */
		void clear();

		/** 
		 * Prints a string to the LCD at the present cursor position. Sends the entire 
		 * string LCDI2C_MAX_STRLEN characters per transmission, rather than one 
		 * character per transmission.
		 *
		 * \param[in]	c	String to be printed
		 */
		void printString(const char c[]);
		/** @}*/

		/** \addtogroup customCharacters Bar Graphs and Custom Characters
		*  @{
		*/

		/**
		 * Initilise the bar graph function 
		 * **caution:** after using this function, custom characters will need to be re-loaded
		 * 
		 * \param graphType 
		 * - 0 = horizontal bar, 
		 * - 1 = horizontal line, 
		 * - 2 = vertical bar	
		 */
		uint8_t initBG(uint8_t);
		
		/**
		 * Draw the bar graph 
		 * 
		 * graphType can be
		 * - 0 = horizontal bar
		 * - 1 = horizontal line
		 * - 2 = vertical bar
		 *
		 * \param graphType
		 * \param row row to start the graph on
		 * \param col column to start the graph on
		 * \param len length of the graph
		 * \param end If bar graph, percentage of the bar to display. If line graph, 
		 *            number of pixels to display
		 */
		uint8_t drawBG(uint8_t graphType, uint8_t row, uint8_t col, uint8_t len, uint8_t end);

		/** 
		 * Load custom characters into the LCDs character memory 
		 * Limit of 8 characters
		 *
		 * **Note:** character memory is used by the bar graph functions, so if the bargraph
		 *           functions are used, custom character will be lost
		 * 
		 * \param[in]	location 	index number to save the custom character to
		 * \param[in]	charmap 	5x8 array discribing the character
		 */
		void customCharacter(uint8_t location, uint8_t charmap[]);

		/** @}*/
		
		/** \addtogroup keypadCommands Keypad Commands
		*  @{
		*/
		
		/**
		 * Controls the mode the 8 bit digital port is operating in.
		 * 
		 * - Mode 0 - Matrix keypad **(default)**
		 * - Mode 1 - 8 separate buttons
		 * - Mode 2 - 8 digital inputs
		 * - Mode 3 - 8 digital outputs
		 * 
		 * **Note:** This feature was intoduced in FW version 1.4, so code will 
		 *           automatically abort if older FW version is specified
		 * \param mode Mode in which the digital port is to be operating in
		 * \return 0 if a valid mode specified, otherwise returns 1
		 */
		bool setKeypadMode(uint8_t mode);
		
		/**
		 * Sends a request for keypad data. The LCD module will return 1 byte of 
		 * keypad data. If there is no keypad data, 0 will be returned. 	
		 * 
		 * \return Contents of the keypad buffer
		 */
		uint8_t readKeypad();
		
		/**
		 * Sends a request for to read the 8 digital inputs. The LCD module will 
		 * return 1 byte of data.
		 *
		 * **Note:** This feature was intoduced in FW version 1.4, so code will 
		 *           automatically abort if older FW version is specified		 
		 * \return Status of the 8 digital inputs
		 */
		uint8_t readDigitalInputs();
		
		/**
		 * Write 1 byte of data to the 8 digial outputs.
		 * 
		 * **Note:** This feature was intoduced in FW version 1.4, so code will 
		 *           automatically abort if older FW version is specified	 
		 * \param data Byte of data to write to the 8 digital outputs
		 */
		void writeDigitalOutputs(uint8_t data);
		
		/** @}*/
		
		/** \addtogroup otherCommands Other Commands
		*  @{
		*/		
		/**
		 * Determine the backlight setting (stored in EEPROM)
		 * **Note:** This feature was intoduced in FW version 1.4, so code will 
		 *           automatically abort if older FW version is specified	 
		 */
		uint8_t readBacklight();
		
		/**
		 * Determine the contrast setting (stored in EEPROM)
		 * **Note:** This feature was intoduced in FW version 1.4, so code will 
		 *           automatically abort if older FW version is specified
		 */
		uint8_t readContrast();
		/** @}*/
	
	private:

		/**
		 * Send a command to the LCD. Overloaded function to cater for varying number of parameters
		 */
		void command(uint8_t);
		void command(uint8_t, uint8_t);
		void command(uint8_t, uint8_t, uint8_t);
		void command(uint8_t, uint8_t, uint8_t, uint8_t);
		void command(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);	

		uint8_t rows; /**< Number of screen rows */
		uint8_t cols; /**< Number of screen columns  */
		uint8_t fw_version; /**< firmware version of the serial backpack  */
		uint8_t i2c_address; /**< i2c address of the serial backpack  */
		uint8_t display; /**< Display number (useful if you have multiple displays/backpacks connected)	 */
		uint8_t keypad_mode; /**< which mode the keypad controller should start in */
		
};

#endif
