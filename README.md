This is my README

Arduino library for the Web4Robots Serial/I2C LCD backpack (operating in I2C mode)

LCDI2C v0.8 27/Apr/2012 peter.feerick@gmail.com	http://www.peterfeerick.com.au

Attribution
===========
Based on initial works by Dale Wentz with contributions from Ran Talbott.

Pin Configuration
=================

Using the I2C (Wiring) interface
- A4 - SDA
- A5 - SCL

Usage
=====

 -- See the examples folder of this library distribution. --
